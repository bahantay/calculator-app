package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class LinearActivity : AppCompatActivity() {

    private lateinit var display: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linear)

        display = findViewById(R.id.display)

        findViewById<Button>(R.id.button_1).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_2).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_3).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_4).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_5).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_6).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_7).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_8).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_9).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_plus).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_minus).setOnClickListener(buttonClickListener)
        findViewById<Button>(R.id.button_multiply).setOnClickListener(buttonClickListener)
    }

    private val buttonClickListener = View.OnClickListener {
        val currentText = display.text.toString()
        val buttonText = (it as Button).text.toString()
        val newText = currentText + buttonText
        display.text = newText
    }
}